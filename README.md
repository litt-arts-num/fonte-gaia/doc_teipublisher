# doc_teipublisher

Documentation et tests partagés pour tei publisher

[[_toc_]]

## Dossiers :


      ./Y
        ├── BARTHELEMY-BLANC_Y.xml      # fichier source de l'édition : j'ai juste ajouté un titre : "Y"
        |
        ├── weller.odd                  # 1 odd chargé dans l'application, à tester / modifier
        |                               # via l'app si vous voulez utiliser l'interface graphique
        |                                
        └── y_simple_translat.html      # le template : dans l'application, j'ai modifié le fichier "view.html"
                                        # en y collant le contenu de 'y_simple_translat.html'


## Instance de test de tei-publisher  ?

* App indépendante générée : https://teipublisher.com/exist/apps/weller-y/index.html *seule manière de pouvoir éditer le template*
* **Pour éditer l'odd via l'interface graphique** :
  1. connectez-vous
  2. cliquez sur l'onglet "fonctions avancées" apparu sur le bandeau supérieur du site
  3. sélectionnez "Éditer l’ODD: weller.odd"


* Accès à l'interface publique d'eXide (modifier le template directement et l'ODD dans le code) sur : https://teipublisher.com/exist/apps/eXide/index.html
* **L'app est publique, tout le monde peut consulter l'édition produite et le code dans la base**, mais pour la modifier / y placer de nouveaux documents, il faut le nom de compte et le mdp que j'envoie par email.



## Template et ODD

Le fichier `y_simple_translat.html` est le template : le patron de la page web dans lequel TEI publisher affiche le document tei source et gère sa transformation en fonction des règles édictées dans l'ODD.
* Le code html est bien plus réduit que celui qui est finalement généré sur la page : tous les éléments préfixés par `<app-..` et `<pb-..` sont des web-components : lorsqu'ils sont affichés, les fonctions sous-jacentes s'exécutent et les "peuplent".
* Ici, les seuls qui nous intéressent réellement sont `<pb-view>` et `<pb-param>`


### Les vues `<pb-view>` 

`<pb-view>` appelle un document source en tei à afficher, et en génère une "vue" .
* L'attribut `@xpath` d'un `<pb-view>` permet de restreindre quelle partie du document est interrogée puis affichée dans cette vue.
* L'attribut `@odd` permet d'associer un ODD particulier à une vue (et donc de mettre en oeuvre les instructions du processing Modle définies dans cet ODD pour chaque élément du fichier source TEI). Ainsi, il est possible d'utiliser plusieurs ODD pour générer plusieurs vue sur un même fichier source ou plusieurs fichier source dans un seul template.
  * Ici, il sert simplement à s'assurer que le bon ODD est appelé pour 2 `<pb-view>` récalcitrants *(la cause des erreurs lors de mes essais : l'interface de test de tei-publisher permet de sélectionner manuellement un ODD à appliquer, mais ne fonctionne pas pour tous les `<pb-view>`)*.


* Dans `y_simple_translat.html` (qui reprend `dantiscus ... ` il y a 4 `<pb-view>`, c'est à dire 4 vues du même document qui vont être retournées :


```xml
<section class="breadcrumbs"> <!-- section d'affichage du fil d'Ariane -->
   <pb-view id="title-view1" src="document1" xpath="//teiHeader" odd="weller" view="single"> <!-- Vue appelée pour remplir le fil d'Ariane : dans notre cas, slmt le titre (mais ce système est prévu pour un affichage "titre oeuvre > chapitre > sous-section consultée > etc." en fonction des instructions passées dans l'ODD )-->
       <pb-param name="header" value="short"/>
   </pb-view>
</section>

<!-- Vue ajoutée pour remplir un cadre avec les métadonnées du //teiHeader, plus détaillées que les infos du fil d'Ariane -->
<pb-view id="metadata" src="document1" xpath="//teiHeader" view="single" odd="weller">
      <pb-param name="header" value="detailed"/>
</pb-view>

<main class="content-body"> <!-- Contenu principal du template -->
    <pb-view  id="view1"  
              src="document1" xpath="//text[@xml:lang = 'la']/body"
              column-separator=".tei-cb" append-footnotes="append-footnotes"
              subscribe="transcription" emit="transcription" view="single"
              /> <!-- 1ère vue du corps du document : demande le texte en latin avec le chemin de @xpath -->
    <pb-view  id="view2"
              src="document1" xpath="//text[@xml:lang='fr']/body"
              column-separator=".tei-cb" append-footnotes="append-footnotes"
              subscribe="transcription" view="single"
              /> <!-- 2ème vue du corps du document : demande le texte en français avec le chemin de @xpath -->

</main>
```                             

>**note** : view="single" défini si le document doit être paginé ou non. La configuration du mode de pagination se fait à l'échelle


### Paramètres externes passés aux vues `<pb-param>`

Cibler certaines sections du document seulement avec `@xpath` ne suffit pas forcément à distinguer les différentes transformations que nous voulons effectuer en fonction des différentes vues. Le fil d'Ariane et le champ de métadonnées, par exemple, ciblent tous deux le contenu de  `//teiHeader`, mais nous ne voulons pas les traiter de la même manière dans ces deux contextes.

Déclarer un `<pb-param>` dans un `<pb-view>` permet de passer un paramètre "externe" à cette vue.

```xml
<pb-view id="metadata" src="document1" xpath="//teiHeader" view="single" odd="weller">
      <pb-param name="header" value="detailed"/> <!-- la vue "metadata" reçoit un paramètre "header" dont la valeur est "detailed" -->
</pb-view>
```

 Dans l'ODD on peut alors utiliser une condition (`<model (s'appliquera si) predicate="la condition en xpath passée ici est vérifiée" >`) pour qu'une transformation ne s'applique que dans le contexte d'une vue dont un paramètre a une certaine valeur :

```xml
<elementSpec ident="teiHeader" mode="change"> <!-- instruction de transformation / mise en forme de teiHeader : -->
      <model predicate="$parameters?header='short'" behaviour="omit"/> <!-- Si la vue a un paramètre "header" ayant pour valeur "short", on n'affiche rien  -->
       <model predicate="$parameters?header='detailed'"> <!-- en revanche, si sa valeur est "detailed", on lance les transformations définies plus bas, que je coupe ici ...  -->
         <!-- ... -->
```

* On a donc une manière simple, à partir d'un seul ODD, de traiter l'information différemment dans plusieurs sections <pb-view> de notre template ! (on aurait pu utiliser plusieurs ODD ... mais restons simples.)
* Et pour gérer un affichage différencié dans ces 4 sections, vous savez désormais qu'il faut passer par un predicate vérifiant :
  * soit la valeur du paramètre externe "header" : `$parameters?header='short'` ou `$parameters?header='detailed'`
  * soit les ancêtres des éléments du texte que l'on est en train de manipuler `ancestor::text[@xml:lang='la']"` ou `ancestor::text[@xml:lang='fr']"`


## Modifier l'ODD


### ODD `y_weller.odd`

* Pour rendre sa modification plus simple, j'ai conçu `y_weller.odd` en copiant le contenu de `teipublisher.odd` qui apparait dans l'interface sous le nom de `Publisher Base`.
* En éditant l'ODD vous aurez donc sous les yeux presque toutes les règles de transformations appliquées.
* *(normalement on ne fait pas comme cela : on le définit comme `@source` de son propre ODD sans en copier directement le contenu, et on se sert des `@mode` pour gérer l'héritage. Mais avoir toutes les règles sous les yeux et agir directement sur elles est bien plus simple pour commencer. `teipublisher.odd` est lui même basé sur `tei_simplePrint.odd` produit par le TEI-C, mais ce dernier contient finalement assez peu de règles de mises en forme.)*
* **Si les règles d'un model s'appliquent étrangement, ou ne s'appliquent pas, essayez de regarder si l'élément ciblé ou ses parents sont déjà mentionnés ailleurs dans l'ODD, et dans celui dont-il hérite : `teipublisher.odd`**


#### Output `<model Output="...">`

**ne va pas vraiment nous intéresser ici** conditionne la transformation en fonction de la sortie (on peut vouloir distinguer web, print et latex (pour générer un beau pdf)). Si rien n'est précisé, la transformation s'applique à toutes les sorties (peut donc ne pas y toucher).


#### description `<model><desc>`

correpond à `<desc>` comme dans un ODD ordinaire : sert à documenter l'élément.


#### predicate  `<model predicate="...">`

* condition d'application en xpath (cf plus haut)
* On peut formuler des conditions complexes facilement avec `and` ou `or`, et `not(condition positive dedans)`

#### behaviour `<model behaviour="...">`

le comportement d'affichage de l'élément. CF : https://teipublisher.com/exist/apps/tei-publisher/doc/documentation.xml?id=behaviours-available. En gros : `block` agit comme <p> : cad que chaque nouveau block est isolé, avec retour à la ligne et des marges configurables en css ... A l'inverse, `inline` affiche dans la continuité ...

* **certains behaviours prennent des paramètres** :
  * `heading` prend un paramètre `level` (`1` à `5`, qui correspond aux `<h1>` -> `<h5>` en html)
  * `note` : `place` qui peut valoir `'footnotes'` ou `'margin'`. Dans le template par défaut il est configuré pour extraire cette valeur de l'attribut `@place` de la note. (si on veut le forcer dans l'ODD, bien entrer une valeur entre guillemets).
  * il y en a beaucoup d'autres dans ce cas

#### Parameters `<model><param name="..." value="..."/>`

* **Tous les models (les règles de transformation), même sans behaviours, peuvent aussi prendre des paramètres** :
  * `content` définit le contenu de l'élément qui est traité : il est toujours sous entendu, même s'il n'est pas déclaré, et par défaut sa valeur est `.` : c'est à dire, tout le contenu du noeud. Mais on peut filtrer pour ne renvoyer qu'une partie du contenu : *regardez le model de `<ab>` pour comprendre pourquoi seule la première ligne de la traduction française s'affiche.*

#### Renditions`<model><outputRenditions xml:space="preserve">` 

* définit des règles CSS pour mettre en forme le contenu de l'élément.
   * regardez le model de l'élément `<l>` du texte latin (`ancestor::text[@xml:lang='la']`) pour voir comment chaque vers est passé en italique et avec une autre police (bien sûr, vous pouvez changer cela).
   * il y a parfois des conflits entre les règles CSS ... si une règle ne fonctionne pas, essayez de la faire suivre de `!important;` pour la faire prévaloir sur les autres et forcer son application (ex : `color: red !important;`)


#### CSS Class `<model cssClass="...">`

Plutôt que de passer des règles à l'unité directement dans l'ODD, associe un élément à une classe CSS qui est définie dans l'élément `/teiHeader/encodingDesc/tagsDecl` de l'ODD :  

```xml
<encodingDesc>
    <tagsDecl>
      <!-- soit en appelant un fichier contenant plusieurs classes CSS -->
        <rendition source="vangogh.css"/>
     <!--soit en définissant directement les classes ici : .choice-alternate est le nom d'une classe. Le nom à entrer dans le champ CSS Class de TEI Publisher est "choice-alternate"   -->                    
        <rendition selector=".choice-alternate">
            display: none;
        </rendition>
        <rendition selector="...">
        </rendition>
      </tagsDecl>

```

### Définir Plusieurs models :


#### L'ordre importe !

* Lorsque plusieurs `<models>` (règles de transformation pour l'affichage) sont définis pour un même élément, ils sont exclusifs. Ce sont donc leurs prédicats (ou des outputs désirés différents) qui permettent de déterminer lequel s'applique.
* **Attention :** les conditions sont testées dans l'ordre dans lequel elles apparaissent dans la définition d'un élément. Il faut donc toujours édicter les plus complexes au départ; Et éventuellement finir par un <model> sans conditions qui s'appliquera à toutes les occurrences d'un élément qui n'ont pas vérifié les conditions précédentes (ce qui reste ... ).  

#### Model et structure des éléments

Un peu comme avec des templates:xsl, définir un model pour un élément n'empêche pas de définir un autre model pour ses enfants, qui resteront ses enfants dans le html généré. Par exemple :
* group/text : a un `behaviour ="body"` : il est le corps du texte affiché
* <ab> a un `behaviour ="block"` : au sein du body, il s'affichera comme un block séparé du reste
* <l> a lui aussi un `behaviour ="block"` : chaque vers formera un petit block au sein du grand block <ab>

Logique ... Mais pour certaines règles css (en particulier passé avec `Rendition`) l'héritage est plus complexe, et les règles passées aux enfants vont remplacer celles de leurs parents.

* Attention : un `behaviour ="omit"` emêche également les enfants de s'afficher.

* **Par défaut, tous les éléments qui ont un modèle défini s'affichent, dans l'ordre dans lequel ils apparaissent dans la source**

#### Models non exclusifs `<modelSequence>`

* Lorsqu'ils sont rassemblés dans une <modelSequence>, les <model> ne sont plus exclusifs mais appliqués à la suite.
* **Cela est utile pour modifier le contenu d'un élément, en s'affranchissant de ce qu'il est dans la source :** on peut appeler dans un nouvel ordre les différents enfants de l'élément contextuel, les dupliquer, ou ne pas les appeler pour ne pas les afficher.

* Une exemple avec teiHeader :
  * Par défaut, le contenu appelé aurait été `.`, donc tous les enfants de teiHeader affichés dans le même ordre que dans la source (leur affichage aurait été géré par leur propre model si un est déclaré pour eux ou des parents direct. Il ne s'affichent pas sinon.)
  * Si on modifie ce content avec le chemin d'un enfant, par ex: `<param name="content" value=".//edition"/>` pour <teiHeader>, seul cet enfant et son contenu s'afficheront : les autres ne seront pas appelés.
  * Pour afficher plus qu'un enfant, on utilise alors modelSequence pour appeler à la suite plusieurs models qui vont chacun chercher un enfant.

* C'est ce que j'ai fait pour teiHeader :

```xml
<elementSpec ident="teiHeader" mode="change">
                <!-- ...  -->
               <modelSequence predicate="$parameters?header='detailed'">
                      <model behaviour="heading">
                           <param name="content" value="'Métadonnées'"/>     <!-- 'Métadonnées' est une valeur passée en dur -->
                            <!-- ... -->
                      </model>
                      <model behaviour="heading">
                        <param name="content" value="./fileDesc/titleStmt/title"/>
                        <param name="level" value="4"/>
                      </model>
                      <model behaviour="block">
                           <param name="content" value=".//edition"/>
                     </model>
                       <model behaviour="block">
                           <param name="encodeur" value="./fileDesc/editionStmt/respStmt/persName"/>
                           <param name="roles" value="string-join(./fileDesc/editionStmt/respStmt/resp  , ', ')"/>
                            <pb:template xmlns="" xml:space="preserve">
                            <p><strong>[[roles]]</strong> : <span>[[encodeur]]</span></p>
                            </pb:template>
                       </model>
                  <!--   <model behaviour="block">
                           <param name="content" value=".//publicationStmt/publisher/orgName"/>
                     </model> -->  <!-- Comme l'ORGNAME est déjà mentionnée dans la description contenue dans /edition,
                                    pas la peine de le répéter ici. Il suffit de ne pas l'appeler, et il ne s'affichera pas. -->
               </modelSequence>
```

* Attention : les enfants appelés peuvent toujours bénéficier de leurs propres <model> qui s'applique (c'est le cas pour edition)

####  Ajouter du contenu. `<pb-templates>`

* `<pb-templates>` permet de créer un modèle personnalisé, et est, je crois, la meilleure solution pour ajouter du texte aux informations extraites de la source.
* il faut d'abord créer des "paramètres" (des variables plutôt) : avec un nom à définir librement, et valeur capturée avec une expression xpath ou xquery
* puis les réinjecter dans une portion de html (ou autre) : exactement comme un template xsl.

* *ici, plutôt que de passer par un `<modelSequence>`, il serait bien plus simple de mettre en forme tout le contenu de l'encadré des métadonnées à l'aide d'un seul model et son `<pb-template>`*

* Attention, dans le cas d'un pb-template, les model des éléments capturés ne s'appliquent pas : ce sont des chaînes de caractère que l'on récupère.

#### Autres solutions (bref extraits de texte)
Il y a 2 autres solutions que j'ai vues employées, mais qui fonctionnent pour des exxtraits plus brefs :
* utiliser un concat() dans un content : `<param name="content" value="concat("./chemin/ de la valeur / a capturer/ , 'chaîne de carac à concatener'")`
* utiliser un petit décorateur CSS (pas mal en préfixe et suffixe ) :

```xml
        <elementSpec ident="edition" mode="change">
            <model predicate="ancestor::teiHeader" behaviour="block">
                  <outputRendition xml:space="preserve" scope="before">
                                          content: 'Edition: ';
                  </outputRendition>  <!-- le contenu de <edition> sera précédé du texte 'Edition: '   -->
```


## A tester dans l'odd :


* Débloquer l'affichage des autres vers de la traduction françaises (un des models de <ab> )
* Changer le style de la police (italique ou non) et même la police :  pour l'instant les polices disponibles sont : Helvetica, Arial, "EB Garamond" (importée en +), "Times New Roman"  ...
* essayer de ne plus afficher les vers avec un behaviour de "block" mais un behaviour "inline" (juste pour voir l'effet)
* compléter le teiheader dans la source et essayer de mettre à jour l'affichage des métadonnées dans leur encadré gris.


* Plus tard :
  * les notes : comment les afficher ?
  * un alignement plus poussé entre le texte et sa traduction. Peut-être en créant des `@xml:id` ou autre forme d'ID aux vers dans le fichier source ?
